#!/bin/sh -x
sed -ne '/<bookinfo/,/<\/bookinfo/p' debian-reference.rawxml >00_bookinfo.rawxml
sed -ne '/<preface/,/<\/preface/p' debian-reference.rawxml >00_preface.rawxml
sed -ne '/<chapter id="_gnu_linux_tutorials"/,/<\/chapter/p' debian-reference.rawxml > 01_gnu_linux_tutorials.rawxml
sed -ne '/<chapter id="_debian_package_management"/,/<\/chapter/p' debian-reference.rawxml > 02_debian_package_management.rawxml
sed -ne '/<chapter id="_the_system_initialization"/,/<\/chapter/p' debian-reference.rawxml > 03_the_system_initialization.rawxml
sed -ne '/<chapter id="_authentication"/,/<\/chapter/p' debian-reference.rawxml > 04_authentication.rawxml
sed -ne '/<chapter id="_network_setup"/,/<\/chapter/p' debian-reference.rawxml > 05_network_setup.rawxml
sed -ne '/<chapter id="_network_applications"/,/<\/chapter/p' debian-reference.rawxml > 06_network_applications.rawxml
sed -ne '/<chapter id="_the_x_window_system"/,/<\/chapter/p' debian-reference.rawxml > 07_the_x_window_system.rawxml
sed -ne '/<chapter id="_i18n_and_l10n"/,/<\/chapter/p' debian-reference.rawxml > 08_i18n_and_l10n.rawxml
sed -ne '/<chapter id="_system_tips"/,/<\/chapter/p' debian-reference.rawxml > 09_system_tips.rawxml
sed -ne '/<chapter id="_data_management"/,/<\/chapter/p' debian-reference.rawxml > 10_data_management.rawxml
sed -ne '/<chapter id="_data_conversion"/,/<\/chapter/p' debian-reference.rawxml > 11_data_conversion.rawxml
sed -ne '/<chapter id="_programming"/,/<\/chapter/p' debian-reference.rawxml > 12_programming.rawxml
sed -ne '/<appendix/,/<\/appendix/p' debian-reference.rawxml >90_appendix.rawxml
